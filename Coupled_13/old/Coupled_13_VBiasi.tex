\documentclass{coupled}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{multicol}

\usepackage[latin1]{inputenc} %% accents
\renewcommand{\vec}[1]{\boldsymbol{#1}}

\title{Thermo-chemical modeling of fiber-polymer composites in fire for fluid/structure interaction}

\author{V. BIASI$^{*}$, G. LEPLAT$^{*}$ AND F. FEYEL$^{\dag}$}

\heading{V. Biasi, G. Leplat and F. Feyel}

\address{$^{*}$ONERA - The French Aerospace Lab\\
F-31055 Toulouse, France\\
e-mail: valentin.biasi@onera.fr, web page: http://www.onera.fr/
\and
$^{\dag}$ONERA - The French Aerospace Lab\\
F-92322 Ch�tillon, FRANCE}

\keywords{Composite materials, Multiphysics Problems, Fire, Pyrolysis, Homogenization}

\abstract{Fiber-polymer composite laminates decompose by pyrolysis or oxidation when submitted to high thermal fluxes. Heat and mass transfer occuring in these materials are investigated for 2D geometries. A multi-components approach is used to determine apparent properties of decomposing materials, depending on component fractions and temperature. The gas flow resulting from degradation reactions is driven by Darcy's law. The degradation of a glass fiber - phenolic resin composite disc exposed to a steady gaussian laser beam is evaluated.}

\begin{document}
%\maketitle


\section{INTRODUCTION}

Composite materials are being used at an increasing rate in aeronautical structures. Their high tenacity, lightweight and corrosion resistance make those materials appropriate in such applications. However their low durability to fire remains one of the major issues restricting their use. Understanding the decomposition of composite materials submitted to fire, with the support of relevant numerical models, will improve the design of composite structures with respect to safety, environmental and cost reduction constraints.

The degradation of such materials under fire is the result of several combined phenomena. Heat transfers can be considered as the result of conductive effects for limited fluxes (less than $20 kW/m^2$) \cite{1991_Milke_Thermalresponseof}. When thermal fluxes are higher, pyrolysis or oxidation transformations appear and modify the material composition. Apparent properties of the composite evolve during the decomposition \cite{2006_Mouritz_FirePropertiesPolymer}, depending on the temperature and the degradation growth. These transformations produce a solid residue and a gas mixture. The latter is carried and ejected outside of the material where this gas phase interacts with the surrounding atmosphere (with possibility of ignition), as shown on figure \ref{fig:phenom_plaque_h_en}.

\begin{figure}[t]
\noindent \fbox{
\hfill\parbox[t]{0.97\textwidth}{
%{\fontsize{11}{13}\fontseries{b}\fontshape{n}\selectfont}
\begin{footnotesize}
{\begin{multicols}{2}[][]
\textbf{Nomenclature} \\
$A$  \hfill \parbox[t]{6.5cm}{Pre-exponential factor} \\
$C_P$  \hfill \parbox[t]{6.5cm}{Heat capacity, J/kg/K} \\
$E_A$  \hfill \parbox[t]{6.5cm}{Activation energy, J/mol} \\
$h$  \hfill \parbox[t]{6.5cm}{Enthalpy, J/kg} \\
$k$  \hfill \parbox[t]{6.5cm}{Thermal conductivity, W/m/K} \\
$K$  \hfill \parbox[t]{6.5cm}{Permeability, m$^2$} \\
$M$  \hfill \parbox[t]{6.5cm}{Molecular mass, kg/mol} \\
$n$  \hfill \parbox[t]{6.5cm}{Order of reaction} \\
$P$  \hfill \parbox[t]{6.5cm}{Pressure, Pa} \\
$Q$  \hfill \parbox[t]{6.5cm}{Heat reaction per unit mass, J/kg} \\
$R$ \hfill \parbox[t]{6.5cm}{Universal gas constant, J/mol/K} \\
$t$  \hfill \parbox[t]{6.5cm}{Time, s} \\
$T$  \hfill \parbox[t]{6.5cm}{Temperature, K} \\
$\vec{v}_g$  \hfill \parbox[t]{6.5cm}{Averaged gas velocity, m/s} \\
$Y$  \hfill \parbox[t]{6.5cm}{Mass fraction} \\
%$ $  \hfill \parbox[t]{6.0cm}{} \\
%$ $  \hfill \parbox[t]{6.0cm}{} \\
%$ $  \hfill \parbox[t]{7cm}{} \\
\textbf{Greek symbols} \\
$\alpha$  \hfill \parbox[t]{6.5cm}{Absorptivity} \\
$\varepsilon$  \hfill \parbox[t]{6.5cm}{Emissivity} \\
$\varphi$  \hfill \parbox[t]{6.5cm}{Volume fraction} \\
$\mu$  \hfill \parbox[t]{6.5cm}{Dynamic viscosity, kg/m/s} \\
$\nu$  \hfill \parbox[t]{6.5cm}{Stoichiometric mass coefficient} \\
$\rho$  \hfill \parbox[t]{6.5cm}{Density, kg/m$^3$} \\
$\dot \omega$  \hfill \parbox[t]{6.5cm}{Reaction rate, kg/m$^3$/s} \\
$ $  \hfill \parbox[t]{7cm}{} \\
\textbf{Subscripts}\\
$0$ - $f$ \hfill \parbox[t]{6.5cm}{Initial state - Final state} \\
$c$ - $v$ \hfill \parbox[t]{6.5cm}{Char - Virgin} \\
$g$ - $s$ \hfill \parbox[t]{6.5cm}{Gas - Solid} \\
$i$ - $J$  \hfill \parbox[t]{6.5cm}{Component - Nb. of components} \\
$j$ - $J$ \hfill \parbox[t]{6.5cm}{Gas component - Nb. of gas components} \\
$m$ - $M$ \hfill \parbox[t]{6.3cm}{Reaction - Nb. of reactions}
\end{multicols}}
\end{footnotesize}
}
\hfill}
\end{figure}


\begin{figure}[!htb]
\begin{center}
\includegraphics[width = 14cm]{img/phenom_plaque_h_en.pdf}
\caption{Schematic of the reaction processes in the through-thickness direction of a decomposing composite during fire exposure}
\label{fig:phenom_plaque_h_en}
\end{center}
\vspace{-1em}
\end{figure}



Several models have been developed for years to represent the decomposition of composite materials under important heat fluxes, mostly based on Henderson et al. \cite{1985_Henderson_ModelThermalResponse,1987_Henderson_MathematicalModelto}. In these models, a composite in degradation is considered as a mix of virgin and charred material. This approach has been modified and completed most notably by Florio et al. \cite{1991_Florio_studyofeffects}, Sullivan and Salomon \cite{1993_Sullivan_FiniteElementMethod,1992_Sullivan_finiteelementmethod}, Dimitrienko \cite{1997_Dimitrienko_Thermomechanicalbehaviourofa} and Galgano et al. \cite{2009_Galgano_Thermalresponseto}. These studies bring essential informations about effects of non-thermal equilibrium, thermal expansion or effective stress modifications. Although these models can accurately describe temperature fields within the material, these studies are limited to 1D cases assuming constant and known heat fluxes.

These assumptions are restrictive for the study of composites in fire because decomposition gases are ejected out of the material and interact with flames. Inflammation of those gases produces an important heat emission \cite{2006_Mouritz_Heatreleaseof} which has an impact on thermal flux received by the material, and so on decomposition.  Multi-dimensional effects of degradation of composites are rarely investigated \cite{2007_Luo_Thermomechanicaldamagemodeling,2012_Luo_Thermomechanicaldamagemodeling} although these are essential to take into account fire interactions including accurate heat flux distribution on impinged surface.

The objective of this work is to propose a model of degradation of composite materials for flame/structure interaction. A major step to achieve this goal is to develop a relevant model which describes thermo-chemical degradation of composite materials under steady heat flux. A bi-dimensional approach is used to model heat and mass transfers in orthotropic materials. Decomposing composites are considered in this model as multi-components materials forming porous media. Pyrolysis and oxidation make solid components react to form gaseous mixtures that are transported through the material up to surfaces. Unsteady interface conditions (heat and mass fluxes, gas mixture composition, temperature and pressure) are the input data that will be used to couple the solid decomposition solver with a Navier-Stokes combustion solver.


\section{THERMO-CHEMICAL MODELING}

A decomposing composite material is represented in this model as a mix of different solid and gas components. Solid components are generally fibers or matrix both at the virgin or charred state, or in a simpler representation the whole solid phase and the charred phase. The gas phase is considered as a mixture of several ideal gases. Throughout this work, a composite material is composed by a set of $I$ components including $J$ gaseous components. The subscript $i$ is used to refer to any component and the subscript $j$ is used to refer only to a gaseous component. %The figure \ref{fig:volumeV_multiconst} shows an example of a mix of composite in degradation in an elementary volume.

% \begin{figure}[!htb]
% \begin{center}
% \includegraphics[width = 7cm]{img/volumeV_multiconst.pdf}
% \caption{Representation of solid and gas components in an elementary volume V}
% \label{fig:volumeV_multiconst}
% \end{center}
% \end{figure}

$\varphi_i$ and $Y_i$ denote respectively the volume fraction and the mass fraction of each $i$ component. $\rho_i$ denotes the absolute density (mass of $i$ divided by volume of $i$) and not the bulk density (mass of $i$ divided by total volume, which could be expressed as $\rho Y_i$). Absolute density of each solid component is assumed constant ($\rho_i=\rho_{i0}$ if $i\in\{s\}$). The weighted density is given by the relation:
\begin{equation}
\rho = \sum \limits_{i=1}^I \rho_i \varphi_i
\label{eq:def_rho}
\end{equation}

The porosity $\varphi_g$ is calculated as the volume fraction of the whole gas phase:
\begin{equation}
 \varphi_g = \sum \limits_{j=1}^J \varphi_j
\label{eq:som_frac_mass_vol_gaz}
\end{equation}


\subsection{Properties}
\label{sec:properties}

The average molecular mass $M$ of the gaseous phase is weighted by gaseous phase volume fractions:
\begin{equation}
M = \frac{1}{\varphi_g} \sum \limits_{j=1}^J \varphi_j M_j
\label{eq:som_frac_mass_mol}
\end{equation}

The surface emissivity $\varepsilon$ and absorptivity $\alpha$ are weighted by solid phase volume fractions, which correspond to surface area fractions of each $i$ solid component:
\begin{equation}
\varepsilon = \frac{1}{\varphi_s} \sum \limits_{i=1}^{I-J} \varphi_i \varepsilon_i \hspace{0.5cm} , \hspace{0.5cm} \alpha = \frac{1}{\varphi_s} \sum \limits_{i=1}^{I-J} \varphi_i \alpha_i \hspace{0.5cm} with \hspace{0.5cm} i\in\{s\}
\label{eq:eplison_alpha}
\end{equation}

\noindent where $\varphi_s$ is the solid volume fraction. Heat capacity $C_P$ of the material depends on the $C_{Pi}$ of each $i$ component weighted by mass fractions:
\begin{equation}
C_P = \sum \limits_{i=1}^I  Y_i C_{Pi}(T)
\label{eq:Cp_moy}
\end{equation}

\noindent and each $C_{Pi}$ depends on temperature using polynomial laws. Sensible enthalpy is calculated, for each component or for the whole material, by integration of heat capacity between the initial temperature $T_0$ and the current temperature $T$:
\begin{equation}
 h_{i} =\int^T_{T_0} {C_{Pi}(\tau)} d\tau  \hspace{1cm} and \hspace{1cm}  h =\int^T_{T_0} {C_{P}(\tau)} d\tau
\label{eq:enthalpie}
\end{equation}


The averaged effective thermal conductivity is defined as a second order tensor. Obviously, any $j$ gas component is considered as isotropic ($\overline{\overline{k_j} } = k_j \overline{\overline{I}}$). For every gas or solid component, each coefficient of the associated tensor depends on temperature using a polynomial law. Fiber diameters and orientations, pore dimensions and distributions are affecting the effective thermal conductivity of the material and those particularities should be taken into account. Due to the lack of experimental characterizations, a law of mixtures (using volume fractions) is used to evaluate $\overline{\overline{k}}$:
 \begin{equation}
\overline{\overline{k}} = \sum \limits_{i=1}^I \varphi_i \overline{\overline{k_i}}
\label{eq:kth}
\end{equation}


\noindent where $\overline{\overline{k_i}}$ is the thermal conductivity tensor of the $i$ component. An ideal gas law is used to calculate the internal pressure as $P=\rho_g \frac{R}{M} T$. Since Reynolds number in these media in degradation are relatively low \cite{2004_Puiroux_TransfertsThermiqueset}, the momentum conservation's law can be homogenized using Darcy's law. The latter links the averaged gas velocity vector $\vec{v}_g$ to the pressure gradient as:
 \begin{equation}
\vec{v}_g = - \frac{K}{\mu_g}\vec{\nabla}P
\label{eq:loi_darcy}
\end{equation}

\noindent where $\mu_g$ is the gas dynamic viscosity and $K$ the permeability. This value follows a geometric average between the initial value $K_{0}$ and the final value $K_{f}$:
\begin{equation}
K = K_{0}^{1-\varphi_g}  K_{f}^{\varphi_g}
\label{eq:evolK}
\end{equation}

\subsection{Chemical reactions}

A set of $M$ chemical reactions is considered to model pyrolysis or oxidation transformations. It is assumed that only solid components can react and that formed gases are inert. The general form of the $m$ reaction is expressed as:

 \begin{equation}
\hspace{0.5em} \nu_{Rm} R_m \hspace{0.5em} + \hspace{0.5em} \nu_{O_2m} \left( O_2 + \lambda N_2 \right) \hspace{0.5em} \stackrel{+Q_m}{\longrightarrow} \hspace{0.5em} \nu_{Pm} P_m \hspace{0.5em} + \hspace{0.5em} \sum \limits_{l=1}^L \left( \nu_{ml} G_{ml} \right) \hspace{1em} +  \hspace{0.5em} \nu_{O_2m} \lambda N_2 \hspace{0.5em}
\label{eq:react_glob}
\end{equation}

\noindent where $R_m$ is a solid reactant, $O_2 + \lambda N_2$ is the air (if present and if oxidation reactions occur), $P_m$ is a solid product and $G_{ml}$ are a gas product (in the set of $L$ gas products emitted in the $m$ reaction). $\nu$ is used to denote stoichiometric mass coefficient and $\lambda$ is the mass ratio of $N_2$ on $O_2$ in the air, that is to say $3.26$.


% $\nu$ is used to denote mass coefficient in the general form of a $m$ reaction. The  latter is expressed in equation \ref{eq:react_glob}, where reactants and products are :
% \begin{itemize}
%  \item $R_m$ a solid reactant
% \vspace{-0.5em}
%  \item $O_2 + \lambda N_2$ the air if present and if oxidation reactions occur
% \vspace{-0.5em}
%  \item $P_m$ a solid product
% \vspace{-0.5em}
%  \item several $G_{ml}$ gas products : a set of $L$ gas products emitted in the reaction $m$.
% \end{itemize}

The heat of reaction $Q_m$ is introduced to express the generated or consumed heat in the $m$ reaction for each consumed quantity of $R_m$. The reaction rate of $R_m$ is driven by an Arrhenius law:
\begin{equation}
 \dot \omega_{Rm} = - f(O_2)\left( \rho_{R} \varphi_R \right)_0 \left( \frac{\rho_R \varphi_R }{\left( \rho_{R} \varphi_R \right)_0 }\right) ^{n_m} A_m . exp\left( \frac{-E_{A_m}}{RT}\right)
\label{eq:arr}
\end{equation}

\noindent where $A_m$, $E_{A_m}$ and $n_m$ are Arrhenius parameters for the $m$ reaction. Terms with the subscript "0" corresponds to quantities at the initial state (before that any degradation reaction occurs). The factor $f(O_2)$ in equation \ref{eq:arr} allows to take into account the oxygen concentration on the reaction rate $\dot \omega_{Rm}$, as:
\begin{equation}
 f(O_2) = 1 \hspace{0.3em} if \hspace{0.3em} \nu_{O_2m} = 0  , \hspace{4em} f(O_2) =  \left(  \frac{\rho_{O_2} \varphi_{O_2}}{\left( \rho_{O_2}  \varphi_{O_2} \right)_0 } \right) ^{n_{O_2m}} \hspace{0.3em} otherwise
\label{eq:fo2}
\end{equation}

The reaction rate $\dot \omega_{Rm}$ is linked to other $\dot \omega_{im}$ reaction rates for each $i$ component which takes part in the $m$ reaction:
\begin{equation}
\dot \omega_{im} = \delta_{im} \frac{\nu_{im}}{\nu_{Rm}}  \dot \omega_{Rm}
\label{eq:dot_omega}
\end{equation}

\noindent where $\delta_{im}$ sets the sign of the reaction rate in equation \ref{eq:dot_omega}: $\delta_{im}=+1$ if $i$ is a reactant and $\delta_{im}=-1$ if $i$ is a product in the $m$ reaction. The total source term of the $i$ component (formation and destruction) is the sum of all reaction rates $\dot \omega_{im}$ where $i$ takes part in the set of $M$ reactions:
\begin{equation}
 \dot \omega_{i} = \sum \limits_{m=1}^M \dot \omega_{im}
\label{eq:prod_gen}
\end{equation}

Another remarkable value is $\left( \dot \omega_{R} Q \right)$, the total heat source produced or consumed by the whole $M$ reactions. The latter is calculated as:
\begin{equation}
 \left( \dot \omega_{R} Q \right) = \sum \limits_{m=1}^M  \dot \omega_{Rm} Q_m
\label{eq:ener_reac}
\end{equation}

\noindent where $Q_m$ is the heat of reaction in $m$.

\subsection{Conservation laws}

All conservation laws in this section correspond to averaged local equations. A mass conservation law is written for each $i$ component (solid or gas). The mass variation is caused only by degradation reactions for solid components, considered as a source term.

\begin{equation}
\frac{\partial}{\partial t} \left( \rho_i \varphi_i \right) = \dot{\omega}_i
\label{cons_msol}
\end{equation}

Concerning gas components, mass variations are also the result of gas production or consumption but as well as bulk gas transport. This term of transport is driven by the pressure gradient as expressed in equation \ref{eq:loi_darcy}. The gas component mass conservation is:
\begin{equation}
\frac{\partial}{\partial t} \left( \rho_j \varphi_j \right) = - \vec{\nabla} \cdot \left( \frac{Y_j}{Y_g} \rho_g \vec{v}_g \right) + \dot{\omega}_j
\label{cons_mgas}
\end{equation}

This equation is also valid for $O_2$ if present. The local thermal equilibrium is assumed in the material, meaning that there are no local temperature difference between all present components. Kinetic energy and pressure work are ignored in the energy conservation.

\begin{equation}
\frac{\partial}{\partial t} \left( \rho h \right) =  - \vec{\nabla} \cdot \left( - k \vec{\nabla} T + h_g  \rho_g \vec{v}_g \right)  +  \left( \dot \omega_{R} Q \right)
\label{cons_ener}
\end{equation}

In this conservation law, the left hand side represents the internal energy variation for all components. The first right hand side term is the contribution of fluxes. The latter is composed of the conduction term and of the energy transport term of the whole gas phase. Finally the last term is the energy source term, contribution of all heat released for the $M$ reactions. These governing equations are forming a set of $I+1$ partial differential equations that has to be solved.


\section{NUMERICAL METHODS}

A numerical solver has been developed for 2D (planar or axisymetric) geometries, using finite volume numerical method to discretize equations developed in \ref{cons_msol}, \ref{cons_mgas} and \ref{cons_ener}. Those equations are integrated over small volumes in unstructured meshes, then divergence terms are converted into surface integrals using the divergence theorem.
Natural variables ($T$, $P$, $\rho$, $\varphi_i$, $Y_i$) are interpolated at surface centers and all properties developed in section \ref{sec:properties} are calculated from those natural variables. Gradients are evaluated at cell centers using the gradient theorem from known values at adjacent cells. The integration in time is performed using an implicit theta-scheme. Since Arrhenius based source terms can cause stability problems due to non linearity and slow characteristic times, source terms are integrated explicitly using sub-time steps.

\section{RESULTS}
\label{s:results}

\subsection{Study case}

An evaluation of the bidimensional solver is performed using a glass fiber - phenolic resin composite material, designated as H41N. Material properties were characterized by Henderson et al. \cite{1987_Henderson_MathematicalModelto} and are summarized in table \ref{tab:param_hend87}. These properties have been established for the virgin and the charred state, and as a consequence three components are used to describe the decomposing composite: "$v$" the virgin component, "$c$" the char component and "$g$" the gaseous phase. Thermal conductivities are arbitrarily modified to illustrate orthotropic heat transfers.

\begin{table}[ht]
\begin{center}
\begin{footnotesize}
\begin{tabular}{llp{4.3cm}|llp{4.3cm}}
\hline
%\mc{6}{c}{Param�tres du mod�le Henderson 1987} \\
%\hline
$\rho_v$ & $[kg/m^3]$ & $2040.6$ & $K_{0}$ & $[m^2]$ & $2.60\times10^{-18}$ \\
%\hline
$\rho_c$ & $[kg/m^3]$ & $1980.7$ & $K_{f}$ & $[m^2]$ & $1.14\times10^{-16}$ \\
$M_g$ & $[kg/mol]$ & $18.35\times10^{-3}$ &  $\varepsilon_{v} - \alpha_v$ & $[-]$ & $0.6$ \\
%\hline
$k_{v_x}$ & $[W/m/K]$ & $1.12 + 3\times10^{-3} T$ & $\varepsilon_{c} - \alpha_c$ & $[-]$ & $0.9$ \\
$k_{v_y}$ & $[W/m/K]$ & $0.35 + 5\times10^{-4} T$ & $\mu_{g}$ & $[kg/m/s]$ & $8.0\times10^{-6} + 2.5\times10^{-8} T$ \\
%\hline
 $k_{c_x}$ & $[W/m/K]$ & $1.12 + 3\times10^{-3} T$ & $\varphi_{g_{ini}}$ & $[-]$ & $0.113$ \\
 $k_{c_y}$ & $[W/m/K]$ & $0.35 + 5\times10^{-4} T$ & $\varphi_{g_{fin}}$ & $[-]$ & $0.274$ \\
 %$k_{c_x}$ & $[W/m/K]$ & $0,31 + 4.3.10^{-3} T - 8,4.10^{-6} T^2 + 5,3.10^{-9} T^3 $ & $\varphi_{g_{ini}}$ & $[-]$ & $0,113$ \\

$k_g$ & $[W/m/K]$ & $-8.4\times10^{-3}+ 1.4\times10^{-4} T$ & $Q$ & $[J/kg]$ & $-234\times10^{3}$ \\
%\hline
$C_{Pv}$ & $[J/kg/K]$ & $791.3 + 1.09 T$ & $A$ & $[-]$ & $1.98\times10^{29}$ \\
%\hline
$C_{Pc}$ & $[J/kg/K]$ & $600.4 + 1.02 T$ & $E_A$  & $[J/mol]$ & $2.6\times10^{5}$  \\
%\hline
$C_{Pg}$ & $[J/kg/K]$ & $2096 + 1.05 T$ & $n$  & $[-]$ & $17.33$  \\
%$\mu_{g}$ & $[kg/m/s]$ & $8,0.10^{-6} + 2.5.10^{-8} T$ & $Q$ & $[J/kg]$ & $234.10^{3}$  \\

\hline
\end{tabular}
\end{footnotesize}
\end{center}
\vspace{-0.4cm}
\caption{Modified properties of H41N (temperatures in Kelvin)}
\label{tab:param_hend87}
\end{table}

The chosen domain is a $80 mm$ diameter and $4.16 mm$ thick disc. A gaussian laser beam impacts the upper face, centered on the symmetry axis. The computational domain is reduced to an axisymmetric rectangle of $40 mm$ along the $\vec{X}$ axis (representing the disc radius) and $4.16 mm$ along the $\vec{Y}$ axis. This domain is splitted into uniform rectangular cells ($1 mm$ along $\vec{X}$ and $0.208 mm$ along $\vec{Y}$) and the time step is fixed at $0.1s$.

All domain boundaries are considered at atmospheric pressure ($P_0 = 101~325 Pa$). On surfaces, convective heat transfers are neglected but radiative heat transfers are considered between surfaces and the surrounding atmosphere temperature (fixed at $293K$). At the initial state, the whole domain is at $T_0 = 293K$ and at atmospheric pressure $P_0$. The surrounding atmosphere is considered as inert (non-oxidative) and only one reaction drives the degradation (where Arrhenius coefficients $E_A$, $A$ and $n$ are detailed in table \ref{tab:param_hend87}). The virgin material "$v$" pyrolyses to form the char material "$c$" and gaseous phase "$g$", with stoichiometric mass coefficients:

\begin{equation}
\hspace{0.5em} v\hspace{0.5em} \stackrel{Q}{\longrightarrow} \hspace{0.5em} 0.795~c \hspace{0.5em} + \hspace{0.5em}  0.205~g
\label{eq:arrh_henderson87}
\end{equation}

Between $t = 0s$ and $t = 400s$, a gaussian heat flux is applied on the upper surface, with a maximal intensity of $318.3 kW/m^2$ along the symmetry axis and a half-width of $10 mm$ (at $1/e^2$).%(length from the axis to where the ratio intensity on maximal intensity is equal to $1/e^2$).



\subsection{Discussions}

\begin{figure}[!htb]
\begin{center}
\includegraphics[width = 12cm]{img/T700_BW.pdf}
\caption{Temperature (right hand side) and pressure (left hand side) distributions associated with averaged gas velocities on surfaces extracted at different times}
\label{fig:T700_BW}
\end{center}
%\vspace{-1em}
\end{figure}
%\vspace{-1em}

\begin{figure}[!htb]
\begin{center}
\begin{minipage}{8.3cm}
\includegraphics[width = 8.3cm]{img/T_y.png}
%\vspace{-0.5cm}
\caption{Temperature and virgin mass fraction along tha symmetry axis}
\label{fig:temp_t700}
\end{minipage}
\hspace{0cm}
\begin{minipage}{7.3cm}
\includegraphics[width = 7.3cm]{img/P_y.png}
\caption{Internal pressure along the symmetry axis}
\label{fig:pression_t700}
\end{minipage}
\end{center}
%\vspace{-2em}
\end{figure}


The figure \ref{fig:T700_BW} shows a mirror view of the domain at four different times after the degradation beginning. Temperature fields are presented on the right hand side and pressure fields on the left hand side of the axis. Averaged gas velocities are represented by vectors only on the domain boudaries. Figure \ref{fig:temp_t700} shows temperatures and virgin mass fractions, and figure \ref{fig:pression_t700} shows internal pressures extracted along the symmetry axis at the same different times detailed in figure \ref{fig:T700_BW}.

Right from the degradation start, temperature is sufficiently high on the front face ($940K$~at $t=50$ seconds) to cause pyrolysis. As a consequence, virgin mass fraction on the front face has considerably decreased after $t=50$ seconds ($Y_v = 0.45$). Virgin component consumption produces gaseous phase, and so the internal pressure increases significantly ($P = 10.4\times 10^5$ Pa at $t=50$ seconds). Formed gases are concentrated around the symmetry axis (see figure \ref{fig:T700_BW}). Atmospheric pressure imposed at all boundary surfaces gives rise to high pressure gradient and so to gas flow through the material.


The pressure peak continues to increase after $100$ seconds up to $P=11.8\times 10^5 Pa$. As shown in figure \ref{fig:T700_BW}, decomposition gases are ejected mostly by the front face and negligibly by the rear face. This difference is explained by permeability values that increase during the decomposition. The pressure peak begins to decrease shortly after $100$ seconds and is equal to $9.6\times 10^5 Pa$ at $t=200$ seconds. At this time, the degradation has reached the rear face as shown by the virgin mass fraction in figure \ref{fig:temp_t700} ($Y_v = 0.98$ at $t=200$ seconds). The gas velocity increases in this zone due to the beginning of the decomposition of the rear face, and continues to raise until $t=400$ seconds. The latter causes other effects such as a gaseous phase dispersion and an internal pressure important decrease ($P=3.6\times 10^5 Pa$ at $t=400$ seconds). On the front or on the rear face, the gas outlet profile tends to spread out of the laser beam (more than $20mm$ width) at $t=400$ seconds. Finally the domain pressure peak is moving out of the symmetry axis along $\vec{X}$.




\section{CONCLUSIONS}

The presented model has been developed with the aim to predict degradations on most fiber-polymer composites submitted to non-uniform fluxes. The multi-components formulation, associated with a specific description of each component, allows to describe detailed transformations into the material. The bidimensional solver is well suited to exhibit the characteristic evolution of the gas ejection profile. The study case developed in section \ref{s:results} shows internal thermo-chemical behavior and expected boundary conditions of a decomposing composite. The revealed informations are essential to prepare the coupling between the decomposing material solver and the simulated flame solver.

\bibliographystyle{unsrt}
\bibliography{/d/vbiasi/BIBLIO/Bib_Val}
% \begin{thebibliography}{99}
% \bibitem{Zienkiewicz}  Zienkiewicz, O.C. and  Taylor, R.L. \textit{The finite element method}. McGraw Hill,
% Vol. I., (1989), Vol. II., (1991).
% \bibitem{Idelsohn} Idelsohn, S.R. and O\~{n}ate, E. Finite element and finite volumes. Two good friends.
% \textit{Int. J. Num. Meth. Engng.} (1994) \textbf{37}:3323--3341.
% \end{thebibliography}

\end{document}


