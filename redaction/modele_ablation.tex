%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%% CHAPITRE MODELE ABLATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Modèle d'ablation}
\label{c:modele_ablation}

\minisommaire

Une des possibilités offertes par la formulation sur maillages non-structurés du solveur MoDeTheC est de pouvoir être appliquée à des géométries complexes mais aussi des maillages déformables. Un stage de fin d'études a été réalisé par B. Kirsch \cite{2014_Kirsch} en vue d'évaluer les possibilités dans ce domaine, notamment pour les matériaux de protection thermique appliqués au spatial où les flux de chaleur sont généralement très importants pour que l'ablation se produise. Cette adaptation nécessite l'ajout de deux fonctionnalités principales au programme support, à savoir la modélisation de la vitesse de régression de la surface déduite de la physique de l'ablation thermique ainsi que l'implémentation du déplacement de la frontière du domaine étudié et de ses conséquences sur le maillage et sur l'état du système.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% SEC. FRONTIRE MOBILE %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section{Stratégie numérique de frontières mobiles}
% \label{s:fontiere_mobile}

Deux grandes familles de méthodes numériques permettant le suivi d'interfaces mobiles ont été identifiées à la suite d'une étude bibliographique :


\begin{itemize}
    \item \textbf{Méthode VOF (Volume Of Fluid)} : largement répandue dans l'étude des écoulements diphasiques hétérogènes, cette méthode permet de reconstruire une interface entre deux phases non-miscibles à l'aide des fractions volumiques de chacune d'entre elles. La méthode VOF présente l'avantage d'être conservative et de travailler sur un maillage fixe. Cependant, l'interface décrite est discontinue, ce qui peut entraîner d'importantes erreurs d'évaluation du flux thermique en paroi.
    
    \vspace{0.5em}
    \item \textbf{Méthode ALE (Arbitrary Lagrangian Eulerian)} : très répandue pour les problématiques fluide/structure à fortes déformations, cette méthode consiste à déformer le maillage en fonction du mouvement de l'interface. En aéroélasticité par exemple, le maillage suit les mouvements de la surface voilure qui est donc lagrangienne tandis que l'écoulement est traité de manière eulérienne. La méthode ALE est plus lourde à mettre en \oe{}uvre que la méthode VOF, puisque l'ensemble des cellules du système doit être déplacé. Malgré cela, un des avantages majeurs est qu'il est possible de décrire des interfaces $\mathcal{C}^1$, c'est-à-dire continues et différentiables, et donc de limiter les erreurs de calcul des flux traversant l'interface du matériau.
\end{itemize}


La figure \ref{fig:ALE_vs_VOF} schématise l'effet des méthodes VOF et ALE sur un maillage simple et un profil de vitesse d'ablation fixé $\vect{V_{ab}}$. La fidélité de la description de la frontière du domaine requise par la détermination des conditions aux limites d'ablation (connaissance du plan tangent à la surface ablatée notamment) est une condition requise pour traiter cette problématique. Cette dernière condition rend complexe l'utilisation de méthodes à maillage fixe dans la mesure où les frontières ne sont pas définies de manière directe mais sont reconstruites à l'aide d'une fonctionnelle. Cette contrainte risque par conséquent de dégrader la description physique du phénomène d'ablation de la surface de l'objet. Le choix se porte donc finalement sur une méthode de type ALE, où la frontière est déplacée par un opérateur lagrangien et les cellules internes s'adaptent aux frontières externes afin d'obtenir une qualité de maillage satisfaisante.


\begin{figure}[!htb]
	\begin{center}
	\hspace{9em}
	\includegraphics[width = 0.55\textwidth]{img/ablation/ALE.pdf}
	\caption{Méthodes numériques de suivi d'interface}
	\label{fig:ALE_vs_VOF}
	\end{center}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% SEC. DEPLACEMENT SOMMET %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Techniques de déplacement du maillage}
\label{s:deplacement_maillage}


Cette section décrit les techniques de déplacement du maillage par une méthode ALE, sans tenir compte du profil de vitesse d'ablation $\vect{V_{ab}}$. Ainsi, à chaque résolution du système d'équations, le maillage est déplacé et optimisé suivant la succession d'étapes détaillées ci-après.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Régularisation des sommets aux limites}
\label{s:regularistion_limite}


Pour chaque limite constitué de $S$ sommets et donc de $S-1$ faces (en 2D), la régression de surface peut entraîner une dégradation de la qualité du maillage. Une première étape consiste à régulariser les sommets de chaque limite ablatée. Une méthode de régularisation des sommets aux limites a été proposée afin d'éviter tout déformation du maillage suite au traitement numérique, telle que schématisée en figure \ref{fig:laplacien_sommet_ablation}.


\begin{figure}[!htb]
        \begin{center}
        \includegraphics[width = 0.85\textwidth]{img/ablation/laplacien_sommet_ablation.pdf}
        \caption{Méthode de régularisation des sommets aux limites}
        \label{fig:laplacien_sommet_ablation}
        \end{center}
\end{figure}

Les étapes suivantes décrivent de manière succincte cette méthode :

\begin{enumerate}
    \item Pour chaque sommet $s$ de la limite, on calcule les tangentes par rapport aux sommets précédent $s-1$ et suivant $s+1$. Pour chaque extrémité, on se limite au premier voisin et à l'extrémité elle-même.
    \item On construit une spline d'Hermite par morceaux. Pour chaque face, une spline cubique est calculée satisfaisant aux conditions de continuité et de différentiabilité (grâce aux calculs des tangentes).
    \item La spline d'Hermite par morceaux est sous-discrétisée en un ensemble de points conservés en mémoire.
    \item L'ensemble des points non-régularisés entre $P_1$ et $P_s$ de la limite est réparti uniformément le long des points de sous-discrétisation. Les sommets $P_1$ et $P_s$ restent fixes lors de cette étape.
\end{enumerate}


\encadre{La discrétisation de l'interface en spline d'Hermite par rapport aux tangentes centrées de chaque sommet est aussi appelée discrétisation en spline de Catmull-Rom.}

%La figure  présente la méthode de régularisation des sommets aux limites.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Correction du déplacement des sommets aux coins}
\label{s:correction_coin}


La particularité des sommets aux coins est qu'ils appartiennent à deux limites distinctes. Il faut donc veiller à ce que le déplacement sur l'une des limites (vitesse d'ablation imposée par exemple) soit compatible avec les contraintes de la limite adjacente (face du solide non ablatée notamment). Une illustration simple de cette problématique consiste à ablater la grande base d'un trapèze par une vitesse verticale constante \cfig{fig:trapeze_ablation}.


\begin{figure}[!htb]
	\begin{center}
	\includegraphics[width = 0.5\textwidth]{img/ablation/trapeze_ablation.png}
	\caption{Méthode de correction du déplacement des sommets aux coins}
	\label{fig:trapeze_ablation}
	\end{center}
\end{figure}

A priori, tous les points de la base sont sensés se déplacer verticalement. Mais si l'on applique une vitesse purement verticale aux deux points d'extrémités on va, en plus de rogner la base du trapèze, déformer ses côtés et créer de la matière fictive (partie jaune hachurée de la figure \ref{fig:trapeze_ablation}). Pour remédier à cela on va corriger le déplacement des sommets aux coins à la courbe interpolée de la limite adjacente par projection.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Déplacement des sommets internes}
\label{s:deplacement interne}


La méthode adoptée pour déplacer les sommets internes (c'est-à-dire n'étant pas placés le long des limites du domaine) repose sur un opérateur laplacien, couramment utilisée pour déformer des maillages (notamment dans des applications de lissage de surface). Cette dernière présente en effet l'avantage de produire des distributions tendant vers l'orthogonalité des mailles (sous réserve que les déformations imposées à la frontière ne soient pas trop sévères) tout en étant intrinsèquement stable.

Soit $P_s$ la position d'un sommet interne $s$, on doit alors résoudre pour l'ensemble des sommets :


\begin{equation}
 \frac{\partial P_s}{\partial t} = \lambda L( P_s )
\label{eq:operateur_laplacien}
\end{equation}


\noi avec $L$ l'opérateur laplacien. Une discrétisation d'ordre 1 de l'opérateur $L( P_s )$ est :

\begin{empheq}[left={L( P_s )\simeq\empheqlbrace}]{align}
\sum_{d=1}^D \frac{x_d - x_s}{|l_{sd}|^2}
\\
\sum_{d=1}^D \frac{y_d - y_s}{|l_{sd}|^2}
\end{empheq}

\noi avec $d$ un sommet voisin de $s$ et $D$ le nombre total de sommets voisins de $s$. $l_{sd}$ représente la longueur du segment entre le sommet $s$ et $d$. Une intégration explicite sur un pas de temps $\Delta t$ entre $t^n$ et $t^{n+1}$ de l'équation linéaire \ref{eq:operateur_laplacien} est telle que :
%Avec ça, on peut intégrer explicitement l'équation de diffusion :

\begin{equation}
 P_s^{n+1} = P_s^n + \Delta t ~\lambda~  L( P_s^n )
\label{eq:avance1}
\end{equation}


Cette intégration explicite est limitée par la condition de stabilité\footnote{La condition de stabilité de l'équation \ref{eq:stab_laplacien} est similaire à une condition de Fourier pour l'équation de la chaleur classique.} :

\begin{equation}
 \Delta t \lambda  \sum_{d=1}^D \frac{1}{|l_{sd}|^2} \leq 1
\label{eq:stab_laplacien}
\end{equation}

Une manière efficace de répondre à cette inégalité est de choisir pour chaque segment un $\lambda_{sd}$ tel que :

\begin{equation}
 \lambda_{sd} = \frac{|l_{sd}|^2}{D \Delta t}
\end{equation}

\noi où $D$ est le nombre de sommets voisins du sommet $s$. %On remarquera que dans cette hypothèse de $\lambda$ non-constant, on devrait faire apparaître des termes supplémentaires dans l'équation de diffusion.
La relation \ref{eq:avance1} peut alors étre reformulée de la manière suivante :

\begin{empheq}[box=\fbox]{align}
 P_s^{n+1} = P_s^n + \frac{1}{D} \sum_{d=1}^D ( P^n_d -  P^n_s )
\label{eq:avance2}
\end{empheq}

On remarque dans l'équation \ref{eq:avance2} que le pas de temps $\Delta t$ a disparu et que cette résolution explicite est inconditionellement stable. Le principe de la méthode consiste à déplacer de manière itérative chaque sommet interne du domaine vers l'isobarycentre de ses sommets voisins. L'opérateur laplacien impose des déplacements de sommets de plus en plus faible à chaque itération, permettant ainsi de définir un critère d'arrêt déterminé par un déplacement seuil relativement aux dimensions des cellules adjacentes.


Néanmoins, cette déformation du maillage intérieur se fait à nombre de sommets constant et ne modifie pas la topologie du maillage. Elle peut s'avérer insuffisante pour simuler des cas d'ablation à très fortes déformations. Par exemple, lorsque la régression de surface s'effectue dans une seule direction, les facteurs de forme des cellules se dégradent au fur et à mesure de l'ablation (les cellules sont « écrasées » par la mouvement de la frontière). De plus, la diminution de la longueur caractéristique de cellule fait augmenter rapidement le nombre de Fourier. L'étude de déformations particulièrement sévères serait alors possible à condition d'ajouter une routine de remaillage conditionnée par des seuils fixés sur les critères évoqués précédemment.

\encadre{Un des critères principaux permettant de s'assurer de la qualité d'un maillage est l'orthogonalité des mailles. En effet, l'orthogonalité des faces d'un maillage avec les vecteurs $\vect{G_1G_2}$ des centres de gravité assure une bonne évaluation des flux (cf. section \ref{ss:calcul_vect_norm} du chapitre \chapterref{c:methodes_numeriques} pour plus d'informations). Ce critère est calculé de la manière suivante :
\vspace{1em}
\begin{equation}
 \text{Critère orthogonalité} = \max \left( \frac{\theta_{max}-\theta_{0}}{\pi-\theta_{0}} , \frac{\theta_{0}-\theta_{min}}{\theta_{0}} \right) \rightarrow 0
\vspace{1em}
\end{equation}
avec $\theta_{0} = \frac{S-2}{S} \pi$ pour un polygone de $S$ sommets. Ce critère a été implémenté dans MoDeTheC afin que l'utilisateur puisse juger de la dégradation de la qualité du maillage au cours de la résolution.
}


La figure \ref{fig:demi_disque} montre un cas test de déformation d'un maillage 2D de demi-disque maillé en triangles où la limite inférieure du demi-disque est ablatée à vitesse constante et uniforme. Ce cas montre les 3 fonctionnalités développées pour le déplacement de maillages avec une interface mobile :
\begin{itemize}
 \item Les sommets extrêmes de la limite inférieure sont corrigés par rapport à la vitesse d'ablation verticale pour continuer à décrire correctement l'arc de cercle définissant la géométrie initiale;
 \item Le reste des sommets aux limites est régularisé pour que les distances curvilignes séparant chacun des sommets soient égales;
 \item Les positions des sommets internes sont optimisées de manière à tendre vers un état d'équilibre de l'opérateur laplacien.
\end{itemize}


\begin{figure}[!htb]
        \begin{center}
        \includegraphics[width = 0.6\textwidth]{img/ablation/demi_disque.png}
        \caption{Cas test de déformation d'un maillage 2D en triangles : le demi-disque}
        \label{fig:demi_disque}
        \end{center}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Conservation des quantités}
\label{s:conservation_quantite}

\`A la suite des étapes de déplacement des sommets du maillage, les variables géométriques (positions des centres de gravités, des aires, des volumes, des vecteurs normaux et des matrices d'interpolation) sont mises à jour, comme cela a été détaillé dans les sections \ref{s:Calcul_des_volumes} et \ref{s:calcul_gradient} du chapitre \chapterref{c:methodes_numeriques}. Il reste cependant une dernière étape de mise à jour des quantités conservées car c'est à partir de ces valeurs qu'il est possible de déterminer l'ensemble des variables naturelles du modèle \cfig{fig:var_nat}.

L'ensemble des équations de conservation rencontrées peuvent se résumer à la forme suivante :

\begin{equation}
\underbrace{ \frac{\partial u}{\partial t} }_{\text{Variation temporelle}} + \underbrace{\nab \cdot \left( u \vect{V} \right)  }_{\text{Advection}} =  \underbrace{ -  \nab \cdot \vect{Q}(u)  }_{\text{Diffusion}} +  \underbrace{ p\left( u \right) }_{\text{Terme source}}
\label{eq:eq_transport_std_abla}
\end{equation}

\noi avec $u$ une inconnue du problème. Or, la déformation des cellules du maillage introduit une vitesse locale de déplacement de celui-ci $\vect{V}_{mesh}$. Les équations de conservation sont modifiées de la manière suivante :


\begin{equation}
\frac{\partial u}{\partial t} + \nab \cdot \left( u \vect{V} \right)   - \underbrace{ \nab \cdot \left( u \vect{V}_{mesh} \right) }_{\text{Déplacement maillage}} =   -  \nab \cdot \vect{Q}(u)+   p\left( u \right)
\end{equation}


L'apparition de ce terme de transport peut être interprétée comme le déplacement des quantités dans le sens opposé au déplacement par rapport au maillage, donc à la vitesse $-\vect{V}_{mesh}$. \'Etant donné que le déplacement des mailles internes est indépendant des équations de conservation, cet opérateur est \textit{splitté} en l'équation de transport classique \ref{eq:eq_transport_std_abla} et l'équation suivante est résolue de manière séparée :

\begin{equation}
\frac{\partial u}{\partial t} =  \nab \cdot \left( u \vect{V}_{mesh} \right)
\end{equation}

Cette équation d'advection classique est linéaire puisque la vitesse $-\vect{V}_{mesh}$ est fixée par l'opérateur laplacien de déformation du maillage. Cette équation est résolue de manière explicite sur le pas de temps $\Delta t$ à l'itération $n$ par la méthode des volumes finis. Ainsi, pour chaque cellule $c$ fermé par $F$ faces, on a :

\begin{equation}
u_{c}^{n+1} = u_{c}^{n} + \Delta t \sum_{f=1}^F u_f^c \vect{V}_{f,mesh}
\end{equation}

\noi où $\vect{V}_{f,mesh}$ est la vitesse de déformation de la face $f$ en son centre de gravité. Cette résolution est soumise à un critère de stabilité $CFL$ classique :

\begin{equation}
CFL = \frac{|\vect{V}_{mesh} | \Delta t}{\Delta L} < 1
\end{equation}

\noi avec $\Delta L$ la dimension caractéristique de la cellule (cf. section \ref{ss:resol_exp_advection} du chapitre \chapterref{c:methodes_numeriques}). Ce critère est assez peu limitant puisque cela sous-entend qu'à chaque pas de temps, une cellule ne peut pas se déplacer au-délà des frontières de son état précédent.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% SEC. MODELISATION NUMERIQUE DE L'ABLATION %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Modélisation de la vitesse d'ablation}
\label{s:modele_vit_ablation}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Ablation à température de paroi fixée}
\label{s:ablation_temp_fixe}


En matière de modélisation des matériaux de protection thermique, une approche très répandue consiste à considérer que l'ablation se fait à température fixée $T_{ab}$ \cite{2006_Koo_ReviewAblationModeling}. L'équation suivante reprend les échanges thermiques classiques de surface d'un matériau avec son environnement, telle qu'elle est implémentée initialement dans MoDeTheC pour chaque face $f$ :

\begin{equation}
f_{Ef}  = \left( - \vect{\Phi_f} \cdot \vect{n_f} + h_{conv} (T_{conv} - T_f ) + \sigma_{SB} ( \alpha_f T_{rad}^4 - \varepsilon_f T_f^4 ) \right) A_f
\end{equation}

\noi avec un flux thermique imposé $\vect{\Phi_f}$ et des échanges convectifs et radiatifs de surface. Cette formulation reste valable tant que la température de surface $T_f$ est inférieure à la température d'ablation $T_{ab}$. Au-delà de cette température, on considère $T_f=T_{ab}$ et un terme supplémentaire de flux ablatif $\Phi_{ab}$ s'ajoute au bilan thermique de surface :

\begin{equation}
f_{Ef}  = \left( - \vect{\Phi_f} \cdot \vect{n_f} + h_{conv} (T_{conv} - T_f ) + \sigma_{SB} ( \alpha_f T_{rad}^4 - \varepsilon_f T_f^4 )  +  {\color{red!70!black}\ \Phi_{ab}}   \right) A_f
\end{equation}


Ce terme correspond à l'excédent énergétique que ne peut supporter le matériau soumis à d'importants flux thermiques. \`A partir de la température $T_{ab}$, une régression de surface apparaît dont l'équivalent énergétique est $\Phi_{ab}$.  Ce flux ablatif est résolu lorsque $T_f \geq T_{ab}$ grâce à la condition de continuité des flux à la paroi. En négligeant le transport énergétique de la phase gazeuse, le flux conductif est égal à l'ensemble des flux échangés avec l'environnement :

\begin{equation}
- \tens{k_f}^* \nab T_f \cdot \vect{n_f}  = - \vect{\Phi_f} \cdot \vect{n_f} + h_{conv} (T_{conv} - T_{f} ) + \sigma_{SB} ( \alpha_f T_{rad}^4 - \varepsilon_f T_{f}^4  )+ \Phi_{ab}
\end{equation}

\noi en imposant  $T_f = T_{ab}$. Il existe aussi une relation directe entre le flux ablatif et la vitesse d'ablation $\vect{V_{ab}}$ de la face $f$ telle que :


\begin{equation}
 \Phi_{ab} = - Q_{ab} ~\rho_f ~\vect{V_{ab}} \cdot \vect{n_f}
\end{equation}

\noi avec $\rho_f$ la masse volumique du matériau exprimé au centre de gravité de la face $f$ et $Q_{ab}$ la chaleur consommée par quantité de matière ablatée. Dans le cas d'un changement de phase, $Q_{ab}$ est considérée comme une chaleur latente et dans le cas d'une réaction hétérogène comme une chaleur de réaction.


Ainsi, ce modèle d'ablation permet de déterminer la régression de surface d'un matériau soumis à d'importants flux thermiques en faisant l'hypothèse que le comportement du matériau est inchangé jusqu'à ce que la température de surface atteigne la température d'ablation. Au-delà, l'excédent énergétique est compensé par les transformations de surface du matériau.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Cas d'application}
\label{s:application_ablation}


Différents cas de validations sur des géométries 1D et 2D sont présentées dans le rapport de B. Kirsch \cite{2014_Kirsch}. Un cas d'application sur un échantillon de matériau composite T700M21 est présenté ici, suivant la configuration du banc laser BLADE telle que détaillée au chapitre \chapterref{c:T700M21_laser}. Ceci a pour but de présenter une première approche de prise en compte de la dégradation totale du matériau suivant les trois réactions successives de dégradation identifiées par ATG. Il est en effet impossible de décrire la dernière réaction de dégradation du matériau comme les précédentes puisque les hypothèses de transferts de chaleur et de masse dans un milieu poreux ne seraient alors plus valables. Le traitement de la réaction finale de dégradation, si aucun constituant solide n'est présent après celle-ci, peut se faire par une régression de surface. Cela présente l'avantage de pouvoir traiter l'ensemble des autres réactions comme des réactions volumiques, comme développé dans la section \ref{s:reac_degradation} du chapitre \chapterref{c:modele_thermo}.


On ajoute au modèle de dégradation thermo-chimique la réaction d'oxydation des fibres à température fixe. On pose $T_{ab}=1000K$ et $Q_{ab}=8.94 \times 10^6 J/kg$. La réaction est alors supposée endothermique. De plus l'intensité du laser a été augmentée à $I_0 = 400kW/m^2$ afin d'atteindre des températures suffisamment hautes pour activer le mécanisme d'ablation en face avant. Le matériau est soumis à ce flux laser de manière continue pendant $500$ secondes.

La figure \ref{fig:T700_abla_phi} montre les champs de fraction volumique de \textit{char} et de gaz après $500$ secondes de dégradation laser, mais aussi la régression de surface subie par le matériau, importante au centre du matériau et peu significative pour $r> 6mm$. La couche de matière ablatée est de $1.0 mm$. Le champ $\varphi_c$ montre que les réactions de pyrolyse et d'oxydation du \textit{char} sont finalisées lors de l'apparition de l'ablation, et donc qu'il ne reste pas de résidu solide après l'oxydation des fibres.

\begin{figure}[!htb]
        \begin{center}
        \includegraphics[width = 0.7\textwidth]{img/ablation/T700_abla_phi.png}
        \caption{Champs de fraction volumique de \textit{char} et de gaz après $500$ secondes de dégradation laser à $I_0=400kW/m^2$}
        \label{fig:T700_abla_phi}
        \end{center}
\end{figure}

Néanmoins, ce modèle d'ablation à température de paroi fixée ne peut être appliqué à des réactions exothermiques comme c'est le cas en réalité lors de la réaction d'oxydation des fibres. L'excédent énergétique pour $T_s \geq T_{ab}$ est traduit par une régression de surface alors que cette réaction doit amener de l'énergie au système. Cette modélisation de l'ablation par oxydation n'a pas été menée à son terme, mais il serait intéressant de chercher une correspondance entre le taux massique d'oxydation des fibres $\dot \omega_f$ (terme volumique) et la vitesse d'ablation $\vect{V}_{ab}$ (terme surfacique).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Bilan}
\label{s:bilan_ablation}
\markright{BILAN}


La méthodologie de déplacement du maillage et des quantités associées via une méthode ALE permet de traiter en théorie tout type de régression de surface du matériau sur des maillages 2D non-structurés. Cette étape est un point clé nécessaire pour toute modélisation de l'ablation. Le modèle d'ablation à température fixée est répandu dans les modélisations de matériaux de protections thermiques spatiaux, mais n'est applicable qu'à des réactions endothermiques. Bien que l'on puisse traiter un grand nombre de problèmes classiques d'ablation avec ce modèle, les réactions exothermiques comme l'oxydation des fibres de carbone exigent un traitement particulier. Des perspectives intéressantes peuvent être amenées pour des applications de matériaux composites à très hautes températures.
